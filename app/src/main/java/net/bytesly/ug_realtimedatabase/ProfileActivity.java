package net.bytesly.ug_realtimedatabase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileActivity extends AppCompatActivity {

    private ImageView imageView;

    TextView textViewName;
    TextView textViewEmail;
    TextView textViewPhone;
    TextView textViewAddress;

    EditText editTextName;
    EditText editTextPhone;
    EditText editTextAddress;
    EditText editTextAvatarUrl;


    Button buttonSave;

    FirebaseAuth auth = FirebaseAuth.getInstance();
    DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("UserInfo");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        init();
        registerListeners();

        if(auth.getCurrentUser() != null) {
            dbRef.child(auth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    UserInfo userInfo = snapshot.getValue(UserInfo.class);

                    if(userInfo == null) {
                        return;
                    }

                    textViewName.setText(userInfo.getName());
                    textViewEmail.setText(userInfo.getEmail());
                    textViewPhone.setText(userInfo.getPhone());
                    textViewAddress.setText(userInfo.getAddress());
                    Glide.with(ProfileActivity.this)
                            .load(userInfo.getAvatarUrl())
                            .placeholder(R.drawable.ic_launcher_foreground)
                            .into(imageView);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }

    }

    private void init() {
        imageView = findViewById(R.id.imageView);
        textViewName = findViewById(R.id.textViewName);
        textViewEmail = findViewById(R.id.textViewEmail);
        textViewPhone = findViewById(R.id.textViewPhone);
        textViewAddress = findViewById(R.id.textViewAddress);

        editTextName = findViewById(R.id.editTextName);
        editTextPhone = findViewById(R.id.editTextPhone);
        editTextAddress = findViewById(R.id.editTextAddress);
        editTextAvatarUrl = findViewById(R.id.editTextAvatarUrl);

        buttonSave = findViewById(R.id.buttonSave);
    }

    private void registerListeners() {
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextName.getText().toString();
                String email = auth.getCurrentUser().getEmail();
                String phone = editTextPhone.getText().toString();
                String address = editTextAddress.getText().toString();
                String url = editTextAvatarUrl.getText().toString();

                UserInfo userInfo = new UserInfo();

                userInfo.setName(name);
                userInfo.setEmail(email);
                userInfo.setPhone(phone);
                userInfo.setAddress(address);
                userInfo.setAvatarUrl(url);

                if(auth.getCurrentUser() != null) {
                    dbRef.child(auth.getCurrentUser().getUid()).setValue(userInfo);
                }
            }
        });
    }
}